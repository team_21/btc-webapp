const webpack = require('webpack')
const path = require('path')

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractCSS = new ExtractTextPlugin('[name].css', { allChunks: true })

const plugins = require('./webpack-config/plugins.config.js').plugins
const outputPath = path.join(__dirname, './src/static/assets/dll')

const vendors = [
  'react',
  'react-dom',
  'react-router',
  'redux',
  'react-redux',
  'isomorphic-fetch',
  'history',
  'moment',
  'semanticUICss'
]

module.exports = {
  devtool: '#source-map',
  context: path.join(__dirname, './src'),
  output: {
    path: outputPath,
    filename: '[name].js',
    library: '[name]_[hash]'
  },
  entry: {
    dll: vendors
  },
  module: require('./webpack-config/module.config.js'),
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
    root: [
      path.resolve(__dirname, './src')
    ],
    alias: {
      semanticUICss: './lib/semantic.min.css'
    }
  },
  plugins: [
    ...plugins,
    extractCSS,
    new webpack.DllPlugin({
      path: path.join(outputPath, 'manifest.[name].json'),
      name: '[name]_[hash]',
      context: path.join(__dirname)
    })
  ]
}
