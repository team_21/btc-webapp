const webpack = require('webpack')
const path = require('path')

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractCSS = new ExtractTextPlugin('style/[name].css', { allChunks: true })

const AssetsPlugin = require('assets-webpack-plugin')
const assetsPluginInstance = new AssetsPlugin({ path: './dist', prettyPrint: true })

const yaml = require('js-yaml')
const fs = require('fs')

const configFile = 'config.yml'
const filePath = path.join(__dirname, configFile)
const appConfig = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'))

const plugins = require('./webpack-config/plugins.config.js').plugins

module.exports = {
  devtool: '#source-map',  // or 'cheap-eval-source-map'
  context: path.join(__dirname, './src'),
  entry: {
    app: ['./client/index.jsx', './client/index.html'],
    vendor: [
      'reactSelectStyle',
      'dearchefIcons',
      'sharedStyle'
    ]
  },
  output: {
    path: path.join(__dirname, './dist/assets/'),
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].chunk.js',
    publicPath: appConfig.STATIC_HOST_NAME + '/assets/',
  },
  module: require('./webpack-config/module.config.js'),
  resolve: require('./webpack-config/resolve.config.js'),
  plugins: [
    ...plugins,
    extractCSS,
    assetsPluginInstance,
    new webpack.DllReferencePlugin({
      context: path.join(__dirname),
      manifest: require('./src/static/assets/dll/manifest.dll.json'),
    }),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'js/[name].js'),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        BABEL_ENV: JSON.stringify(process.env.BABEL_ENV || 'development:client'),
        BUILD: JSON.stringify(process.env.BUILD || 'dev'),
        BROWSER: true
      }
    })
  ],
  devServer: {
    contentBase: './dist',
    hot: true
  },
  externals: {
    APP_CONFIG: JSON.stringify(appConfig)
  },
  cache: true
}
