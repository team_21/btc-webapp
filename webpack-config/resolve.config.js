const path = require('path')
const nodeModulesPath = path.join(__dirname, '../node_modules')
const rootDir = path.resolve(__dirname, '../src')

module.exports = {
  extensions: ['', '.js', '.jsx', '.json'],
  root: [
    rootDir
  ],
  alias: {
    reactSelectStyle: `${nodeModulesPath}/react-select/dist/react-select.min.css`,
    dearchefIcons: './lib/dearchef-icons/style.css',
    sharedStyle: './lib/sharedStyle.css'
  }
}