const webpack = require('webpack')
const os = require('os')

const HappyPack = require('happypack')
HappyPack.SERIALIZABLE_OPTIONS = HappyPack.SERIALIZABLE_OPTIONS.concat(['postcss'])
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length })

module.exports = {
  plugins: [
    new HappyPack({
      id: 'js',
      loaders: ['babel?cacheDirectory=true'],
      threadPool: happyThreadPool,
      cache: true,
      verbose: true
    }),
    new HappyPack({
      id: 'css',
      loaders: ['css?modules&minimize&import&sourceMap&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]!postcss'],
      threadPool: happyThreadPool,
      cache: true,
      verbose: true
    }),
    new HappyPack({
      id: 'excludeStyle',
      loaders: ['css?modules&minimize&sourceMap&importLoaders=1&localIdentName=[local]!postcss'],
      threadPool: happyThreadPool,
      cache: true,
      verbose: true
    })
  ]
}

