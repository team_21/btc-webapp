const ExtractTextPlugin = require('extract-text-webpack-plugin')
const excludeStyle = /(react\-select\.min|semantic\.min|dearchef\-icons\/style|sharedStyle|react\-widgets|rc-drawer\/assets\/index|dashboard\-calendar|react\-datepicker|react\-day\-picker\/lib\/style)\.css/

module.exports = {
  loaders: [
    {
      test: /\.html$/,
      loader: 'html-loader'
    },
    {
      test: /\.css$/,
      include: [excludeStyle],
      loader: ExtractTextPlugin.extract('style-loader', 'happypack/loader?id=excludeStyle')
    },
    {
      test: /\.css$/,
      exclude: [excludeStyle],
      loader: ExtractTextPlugin.extract('style-loader', 'happypack/loader?id=css')
    },
    {
      test: /\.less$/,
      loader: 'style-loader!css-loader!less-loader'
    },
    {
      test: /\.(jpg|png|gif|svg)(|\?[a-z0-9=\.]+)?$/,
      loader: 'url-loader?name=[path][name]-[hash:base64:5].[ext]&prefix=images/'
    },
    {
      test: /\.(otf|eot|ttf|woff|woff2)(|\?[a-z0-9=\.]+)?$/,
      loader: 'url-loader?name=[path][name]-[hash:base64:5].[ext]&prefix=font/'
    },
    {
      test: /\.pug$/,
      loaders: ['pug-loader']
    },
    {
      test: /\.json$/,
      exclude: /node_modules/,
      loaders: ['json-loader']
    },
    {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loaders: [
        // 'babel-loader'
        'happypack/loader?id=js'
      ]
    }
  ]
}