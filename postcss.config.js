const webpack = require('webpack')
const rucksack = require('rucksack-css')
const postcssImport = require('postcss-import')
const precss = require('precss')

module.exports = {
  plugins: [
    postcssImport({
      addDependencyTo: webpack
    }),
    rucksack({
      autoprefixer: true,
      rucksack: true
    }),
    precss
  ]
}
