import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { syncHistory } from 'react-router-redux'
import createLogger from 'redux-logger'
import rootReducer from '../reducers'
import api from '../middleware/api'

export default function configureStore (initialState, history) {
  const reduxRouterMiddleware = syncHistory(history)

  const finalCreateStore = (process.env.NODE_ENV === 'development')
    ? compose(
        applyMiddleware(thunk, api, reduxRouterMiddleware),
        applyMiddleware(createLogger({ logger: console }))
      )(createStore)
    : compose(
        applyMiddleware(thunk, api, reduxRouterMiddleware)
      )(createStore)

  const store = finalCreateStore(rootReducer, initialState)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }

  reduxRouterMiddleware.listenForReplays(store)
  return store
}
