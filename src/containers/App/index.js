import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import classnames from 'classnames'
import style from './style.css'

class App extends Component {

  render() {
    const { children } = this.props
    return (
      <div id="app">
        { children }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing
  }
}

function mapDispatchToProps(dispatch) {
  return {
    routerActions: bindActionCreators({ push, replace }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)