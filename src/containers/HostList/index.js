import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import classnames from 'classnames'
import style from './style.css'
import Header from 'components/Header'

class HostList extends Component {

  render() {
    const { user } = this.props
    return (
      <div >
        <Header />
        <div className="ui container">
          <h2 className="ui header">Recommended Local Guides</h2>
          <div className="ui divided selection list">
            {
              user.users.map((person, i) => 
              (<div className="item" key={"person"+i}>
                <div className="right floated content tright">
                  <p className="header">{person.distance}</p>
                  <div className={style.section}>
                    <div className="description ui label">{person.section}</div>
                  </div>
                </div>
                <img className="ui avatar image tiny" src={"/assets/images/avatars/"+(i+1)+".jpg"} />
                <div className={classnames("content", style.userInfo)}>
                  <p className={classnames("header", style.username)}>{person.name}</p>
                  <div className={classnames("description", style.brief)}>{person.brief}</div>
                </div>
              </div>)
            )}
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    routerActions: bindActionCreators({ push, replace }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HostList)