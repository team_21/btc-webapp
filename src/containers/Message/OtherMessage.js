import React from 'react'
import moment from 'moment'
import classnames from 'classnames'
import style from './style.css'

const OtherMessage = ({ item }) => {
  const createdDateTime = item.datetime
    ? moment(item.datetime).format('MMMM Do H:mm') : 'Sending'
  const createdDateTimeFull = item.datetime
    ? moment(item.datetime).format('LLLL') : 'Sending'
  return (
    <div className={classnames(style.item, 'item')}>
      <div className={classnames(style.message, style.other)}>
        <div
          className={style.datetime}
          title={createdDateTimeFull}
        >
          {createdDateTime}
        </div>
      </div>
      <div className={classnames(style.feed, 'ui feed', style.other)}>
        <div className='ui avatar image'>
          <img src={item.userPhoto} />
        </div>
        <span className={classnames(style.content, 'content')}>
          <span className={style.userName}>{item.userName}</span>
        </span>
      </div>
    </div>
  )
}

export default OtherMessage
