import React from 'react'
import moment from 'moment'

const SystemMessage = ({ item }) => (
  <div className='item'>
    <div className='ui horizontal divider'>
      {`${moment(item.datetime).format('LL')} ${item.message}`}
    </div>
  </div>
)

export default SystemMessage
