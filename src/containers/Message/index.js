import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import classnames from 'classnames'
import style from './style.css'

import SystemMessage from './SystemMessage'
import OtherMessage from './OtherMessage'
import MyMessage from './MyMessage'
import TextArea from 'components/TextArea'

import * as _messageActions from 'actions/message'

class Message extends Component {
  render () {
    return (
      <div>
        <ChatBoard />
      </div>
    )
  }
}

class ChatBoard extends Component {
  componentDidMount() {
    // window.chatSocket.connect()
  }

  componentWillUnmount() {
    // window.chatSocket.disconnect()
  }

  // componentWillReceiveProps(nextProps) {
  //   if (this.props.im.socketId !== nextProps.im.socketId)
  //     this.initChat()
  // }

  // initChat() {
  //   const { actions, roomId, accessToken } = this.props
  //   actions.initChat({ roomId, accessToken })
  // }

  handleMessageList() {
    const { actions, roomId, roleType, im } = this.props
    const { nextKey } = im
    const query = { roleType, roomId }
    if (nextKey) query.nextKey = nextKey

    actions.roomMessageList(query)
  }

  handleSave(text) {
    this.props.actions.editTextInput(text)
  }

  handleSubmit() {
    const { im, actions, roleType } = this.props
    const text = im.textInput.trim()
    if (text === '') return

    actions.newMessage({
      id: uuid.v4(),
      msgType: roleType,
      message: text
    })
  }

  render() {
    const { im, roleType, chatroomData, t } = this.props
    const { isSocketOK, messageData, nextKey, isFetching, textInput } = im

    return (
      <div className={classnames(style.segments, 'ui segments')}>
        <div className="ui padded segment">
          <Dimmer isShow={!isSocketOK} />
          <div className="ui list">
          {
            messageData.map((item, i) => {
              switch (item.msgType) {
                case 'system':
                  return <SystemMessage
                            key={`${item.datetime}-${i}`}
                            item={item}
                          />
                case 'me':
                  return <MyMessage
                            key={`${item.datetime}-${i}`}
                            item={item}
                          />
                case 'other':
                  return <OtherMessage
                            key={`${item.datetime}-${i}`}
                            item={item}
                          />
                default:
                  return null
              }
            })
          }
          </div>
        </div>
        <div className="ui secondary segment">
          <form className="ui form">
            <div className={classnames(style.fields, 'inline fields')}>
              <div className={classnames(style.field, 'sixteen wide field')}>
                <TextArea
                  ref="textArea"
                  rows="1"
                  text={textInput}
                  onSave={text => { this.handleSave(text) }}
                  dynamicRow={true}
                />
                <button
                  type="button"
                  onClick={::this.handleSubmit}
                  className={classnames(style.submitButton, 'ui red button')}
                >
                  Send
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    routing: state.routing,
    message: state.message
  }
}

function mapDispatchToProps (dispatch) {
  return {
    routerActions: bindActionCreators({ push, replace }, dispatch),
    actions: bindActionCreators(_messageActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Message)