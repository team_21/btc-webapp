import React, { Component, PropTypes } from 'react'

class TextArea extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      text: this.props.text || '',
      rows: this.props.rows || 1
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.text !== this.props.text)
      this.setState({ text: nextProps.text })
  }

  handleChange(e) {
    const text = e.target.value
    this.setState({ text })
    this.props.onSave(text)
  }

  handleBlur(e) {
    this.handleChange(e)
  }

  handleKeyUp(e) {
    const { dynamicRow } = this.props
    if (dynamicRow && (e.keyCode === 13 || e.keyCode === 8)) {
      this.setState({
        rows: this.state.text.split('\n').length
      })
    }
  }

  render() {
    return (
      <textarea
        ref="textArea"
        rows={this.state.rows}
        cols={this.props.cols || '100'}
        className={this.props.className}
        placeholder={this.props.placeholder}
        value={this.state.text}
        onBlur={::this.handleBlur}
        onChange={::this.handleChange}
        onKeyUp={::this.handleKeyUp}
      />
    )
  }
}

TextArea.propTypes = {
  onSave: PropTypes.func,
  text: PropTypes.string,
  placeholder: PropTypes.string
}

export default TextArea