import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import style from './style.css'

class App extends Component {

  render() {
    return (
      <div className={style.navBar}>
        <div className="ui container">
          <img className={style.logo} src="assets/images/header/logo.png" />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing
  }
}

function mapDispatchToProps(dispatch) {
  return {
    routerActions: bindActionCreators({ push, replace }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)