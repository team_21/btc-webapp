import React from 'react'
import { Route, IndexRoute, IndexRedirect } from 'react-router'

import App from 'containers/App'
import HostList from 'containers/HostList'


export default function () {
  return (
    /* eslint-disable global-require, max-len*/
    <Route path="/" component={App} >
      <Route path="/hostlist" component={HostList} />
    </Route>
    /* eslint-enable global-require, max-len*/
  )
}
