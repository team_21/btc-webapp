import fs from 'fs'
import path from 'path'
import APP_CONFIG from '../config'
const { CREDENTIALS_FOLDER, PRIVATE_KEY_FILENAME, CERTIFICATE_FILENAME, CA_CHAIN_FILENAME } = APP_CONFIG

const chainLines = fs.readFileSync(path.join(__dirname, `${CREDENTIALS_FOLDER}/${CA_CHAIN_FILENAME}`), 'utf8').split('\n')

let cert = []
let ca = []

chainLines.forEach(line=> {
  cert.push(line)
  if (line.match(/-END CERTIFICATE-/)) {
    ca.push(cert.join('\n'))
    cert = []
  }
})

const options = {
  key: fs.readFileSync(path.join(__dirname, `${CREDENTIALS_FOLDER}/${PRIVATE_KEY_FILENAME}`), 'utf8'),
  cert: fs.readFileSync(path.join(__dirname, `${CREDENTIALS_FOLDER}/${CERTIFICATE_FILENAME}`), 'utf8'),
  ca: ca
}

const ssl = {
  options: options
}

export default ssl