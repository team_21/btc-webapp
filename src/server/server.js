import Express from 'express'
import path from 'path'
import fetch from 'isomorphic-fetch'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { RouterContext, match, createMemoryHistory } from 'react-router'

import APP_CONFIG from '../config'
const { DEFAULT_PORT, RESOURCE_HOST_NAME, TAG_MANAGER_ID, FB_APP_ID, FB_MESSENGER_URL, HOST_NAME, API_ROOT } = APP_CONFIG

import { I18nextProvider, loadNamespaces } from 'react-i18next'
import i18n from './i18n'
const i18nMiddleware = require('i18next-express-middleware')
import compression from 'compression'
import Promise from 'bluebird'

import configureStore from '../store'
import createRoutes from '../routes'
import { Provider } from 'react-redux'

import cookieParser from 'cookie-parser'
import reactCookie from 'react-cookie'

import webpackAssets from 'express-webpack-assets'
import Helmet from 'react-helmet'

import http from 'http'
import sm from 'sitemap'

const server = new Express()
const port = DEFAULT_PORT || 3000
server.use(i18nMiddleware.handle(i18n))
server.set('port', port)

server.use(compression())
server.use(cookieParser())

const oneDay = 86400000
server.use(Express.static(path.join(__dirname, '../../dist'), { maxAge: oneDay * 30 }))

const assetsPluginFile = (process.env.NODE_ENV === 'production')
  ? 'webapp-assets.' + (process.env.BUILD) + '.json' : 'webpack-assets.json'

server.use(webpackAssets(path.join(__dirname, '../../dist/' + assetsPluginFile), {
  devMode: (process.env.NODE_ENV === 'developement')
}))
server.set('views', path.join(__dirname, './views'))
server.set('view engine', 'pug')

function createSitemap () {
  const options = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  }

  return fetch(`${API_ROOT}/indexPageListWithManual`,
    { ...options, method: 'GET' })
    .then(response => response.json())
    .then(json => {
      const urls = []
      if (json.data && json.data.length > 0) {
        const parties = json.data
        for (let i = 0; i < parties.length; i++) {
          urls.push({
            url: `party/${parties[i].partyId}`,
            changefreq: 'daily',
            priority: 0.9,
            img: parties[i].cover || ''
          })
        }
      }

      return sm.createSitemap({
        hostname: HOST_NAME,
        cacheTime: 600000, // 600 sec - cache purge period
        urls: [...urls,
          { url: '/terms/service', changefreq: 'monthly', priority: 0.3 },
          { url: '/terms/privacy', changefreq: 'monthly', priority: 0.3 },
          { url: 'http://blog.dearchef.tw/', changefreq: 'monthly', priority: 0.8 }
        ]
      })
    })
}

server.get('/sitemap.xml', (req, res) => {
  createSitemap()
  .then(sitemap => {
    sitemap.toXML((err, xml) => {
      if (err) return res.status(500).end()

      res.header('Content-Type', 'application/xml')
      return res.send(xml)
    })
  })
})

server.get('*', (req, res) => {
  const history = createMemoryHistory(req.url)
  const store = configureStore(undefined, history)
  const routes = createRoutes()
  const location = history.createLocation(req.url)
  const fullUrl = HOST_NAME + req.originalUrl
  const locale = req.language
  const resources = i18n.getResourceBundle(locale, 'common')
  const i18nClient = { locale, resources }
  const i18nServer = i18n.cloneInstance()
  i18nServer.changeLanguage(locale)

  reactCookie.plugToRequest(req, res)
  reactCookie.setRawCookie(req.headers.cookie)

  function getReduxPromise ({ query, params, components }) {
    const comp = components[components.length - 1].WrappedComponent
    const promise = comp && comp.fetchData ?
      comp.fetchData({ query, params, store, history, i18n: i18nServer }) :
      Promise.resolve()
    return promise
  }

  function renderApp(renderProps) {
    // let [ getCurrentUrl, unsubscribe ] = subscribeUrl()
    getReduxPromise(renderProps)
      .then(() => {
        const reduxState = escape(JSON.stringify(store.getState()))
        const html = ReactDOMServer.renderToString(
          <I18nextProvider i18n={i18nServer}>
            <Provider store={store}>
              <RouterContext history={history} {...renderProps} />
            </Provider>
          </I18nextProvider>
        )
        const head = Helmet.rewind()
        const DLL_CSS = (process.env.NODE_ENV === 'production')
          ? `${RESOURCE_HOST_NAME}/assets/dll/dll.min.css`
          : `${RESOURCE_HOST_NAME}/assets/dll/dll.css`
        const DLL_JS = (process.env.NODE_ENV === 'production')
          ? `${RESOURCE_HOST_NAME}/assets/dll/dll.min.js`
          : `${RESOURCE_HOST_NAME}/assets/dll/dll.js`
        res.render('index',
          {
            head,
            i18n: i18nClient,
            html, reduxState,
            fullUrl,
            DLL_CSS,
            DLL_JS,
            TAG_MANAGER_ID,
            FB_APP_ID,
            FB_MESSENGER_URL
          }
        )
      })
      .catch(error => {
        console.error(error)
        res.status(404).redirect('/notFound')
      })
  }

  match({ history, routes, location },
  (error, redirectLocation, renderProps) => {
    // loadNamespaces({ ...renderProps, i18n: i18nServer })
    // .then(() => {
      if (redirectLocation) {
        const url = redirectLocation.pathname + redirectLocation.search
        res.status(301).redirect(url)
      } else if (error)
        res.status(500).send(error.message)
      else if (renderProps)
        renderApp(renderProps)
      else
        res.status(404).redirect('/notFound')
    // })
  })

  // function subscribeUrl () {
  //   let currentUrl = location.pathname + location.search;
  //   let unsubscribe = history.listen((newLoc)=> {
  //     if (newLoc.action === 'PUSH') {
  //       currentUrl = newLoc.pathname + newLoc.search;
  //     }
  //   })

  //   return [
  //     ()=> currentUrl,
  //     unsubscribe
  //   ]
  // }
})


const httpServer = http.createServer(server).listen(port)
console.log(`WebApp server is listening to port: ${port}`)