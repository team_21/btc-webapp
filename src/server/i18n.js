import i18n from 'i18next'
import i18nFsBackend from 'i18next-node-fs-backend'
import { LanguageDetector } from 'i18next-express-middleware'

const detectOption = {
  // order and from where user language should be detected
  order: [/* 'path', 'session', */ 'querystring', 'cookie', 'header'],
  // keys or params to lookup language from
  lookupQuerystring: 'lng',
  lookupCookie: 'i18next',
  // lookupSession: 'lng',
  // lookupPath: 'lng',
  // lookupFromPathIndex: 0,

  // cache user language
  // caches: false, // ['cookie']
  caches: ['cookie']

  // optional expire and domain for set cookie
  /* cookieExpirationDate: new Date(),
  cookieDomain: 'myDomain' */
}

i18n
  .use(i18nFsBackend)
  .use(LanguageDetector)
  .init({
    whitelist: ['en', 'zh-TW'],
    detection: detectOption,
    // detection: (process.env.BUILD === 'prod') ? {} : detectOption,
    // lng: (process.env.BUILD === 'prod') ? 'zh-TW' : null,
    fallbackLng: 'zh-TW',
    useCookie: true,
    cookieName: 'i18next',
    // have a common namespace used around the full app
    ns: ['common'],
    defaultNS: 'common',
    debug: false,
    interpolation: {
      escapeValue: false // not needed for react!!
    }
  })


export default i18n