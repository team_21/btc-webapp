const hook = require('css-modules-require-hook')
const lessParser = require('postcss-less').parse

hook({
  rootDir: './src',
  extensions: ['.less', '.css'],
  generateScopedName: '[name]__[local]__[hash:base64:5]',
  processorOpts: { parser: lessParser }
})

require('app-module-path').addPath(__dirname + '/../')
require('babel-core/register')
require('./server.js')

