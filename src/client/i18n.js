import i18n from 'i18next'
import XHR from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

const detectOption = {
  // order and from where user language should be detected
  // order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag'],
  order: ['querystring', 'cookie', 'navigator', 'htmlTag'],
  // keys or params to lookup language from
  lookupQuerystring: 'lng',
  lookupCookie: 'i18next',
  // lookupLocalStorage: 'i18nextLng',
  // cache user language on
  caches: ['cookie'],
  // optional expire and domain for set cookie
  // cookieMinutes: 10,
  // cookieDomain: 'myDomain',
  // optional htmlTag with lang attribute, the default is:
  // htmlTag: document.documentElement
}

i18n
  .use(XHR)
  .use(LanguageDetector)
  .init({
    whitelist: ['en', 'zh-TW'],
    detection: detectOption,
    // detection: (process.env.BUILD === 'prod') ? {} : detectOption,
    // lng: (process.env.BUILD === 'prod') ? 'zh-TW' : null,
    fallbackLng: 'zh-TW',
    useCookie: true,
    cookieName: 'i18next',
    // have a common namespace used around the full app
    ns: ['common'],
    defaultNS: 'common',
    debug: false,
    interpolation: {
      escapeValue: false // not needed for react!!
    },
    // load: 'currentOnly',
    backend: {
      // 設定語系檔案的 server 路徑, 會以GET的方式跟 server 要檔案
      // lng = 語系代碼 ns = namespace
      loadPath: '/locales/{{lng}}/{{ns}}.json',
      jsonIndent: 2
    }
    // getAsync: false
  })

export default i18n