import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import React from 'react'
import { I18nextProvider } from 'react-i18next'
import i18n from './i18n'
import createRoutes from '../routes'
import configureStore from '../store'
import { Router, useRouterHistory, match, applyRouterMiddleware } from 'react-router'
import { createHistory } from 'history'
import useScroll from 'react-router-scroll'

const history = useRouterHistory(createHistory)()
const serverSideState = (window.__INITIAL_STATE__)
      ? JSON.parse(unescape(window.__INITIAL_STATE__))
      : {}
const initState = serverSideState
const routes = createRoutes()
const store = configureStore(initState, history)
const useScrollMiddleware = useScroll((prevRouterProps, { location }) => (
  prevRouterProps && location.pathname !== prevRouterProps.location.pathname
))

match({ history, routes }, (error, redirectLocation, renderProps) => {
  ReactDOM.render(
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <Router
          history={history} {...renderProps}
          render={applyRouterMiddleware(useScrollMiddleware)}
        />
      </Provider>
    </I18nextProvider>,
    document.getElementById('root')
  )
})