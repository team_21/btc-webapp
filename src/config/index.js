let config = null

if (process.env.BROWSER) {
  config = require('APP_CONFIG')
} else {
  config = require('./loadConfig.js').default
}

export default config