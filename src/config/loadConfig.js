import yaml from 'js-yaml'
import path from 'path'
import fs from 'fs'

const configFile = (process.env.BUILD) ? `config.${process.env.BUILD}.yml` : `config.yml`
const filePath = path.join(__dirname, '..', '..', configFile)
const config = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'))

export default config