import { combineReducers } from 'redux'
import { routeReducer as routing } from 'react-router-redux'
import user from './user'

export default combineReducers({
	user,
  routing
})
