import { handleActions } from 'redux-actions'

const initState = {
  users: [
    {
      name: 'Jeff',
      distance: '1hr',
      brief: 'Walk around the local market',
      section: 'Bai-Tou'
    },
    {
      name: 'Jane',
      distance: '30min',
      brief: 'Go nearby bar together',
      section: 'Shin-Yi'
    }
  ]
}

export default handleActions({
}, initState)