import fetch from 'isomorphic-fetch'

import APP_CONFIG from '../config'
const { API_ROOT } = APP_CONFIG

export const CALL_API = Symbol('CALL_API')

function checkStatus(response) {
  if ((response.status >= 200 && response.status < 300)
    || response.status === 400)
    return response

  const error = new Error(response.statusText)
  error.response = response
  throw error
}

function giveFullUrl(endpoint) {
  if (endpoint.indexOf('http://') !== -1 || (endpoint.indexOf('https://') !== -1))
    return endpoint

  return (endpoint.indexOf(API_ROOT) === -1)
    ? `${API_ROOT}/${endpoint}`
    : endpoint
}

export default store => next => action => {
  if (!action[CALL_API]) return next(action)

  const callAPI = action[CALL_API]
  const { _pointer, method, headers, endpoint, query, upload, types, shouldNeedAuth } = callAPI
  const [requestType, receiveType, failureType] = types
  const { dispatch } = store
  const userLanguage = store.getState().user.language

  dispatch({ type: requestType, payload: query, _pointer })

  const fullUrl = giveFullUrl(endpoint)
  let options =
    { method,
      headers: {
        ...headers,
        Accept: 'application/json; charset=UTF-8'
      }
    }

  if (query && (
    method.toUpperCase() === 'POST' ||
    method.toUpperCase() === 'PUT'  ||
    method.toUpperCase() === 'DELETE')) {
    options = { ...options,
      headers: { ...options.headers,
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: JSON.stringify(query)
    }
  }

  if (upload && (
    method.toUpperCase() === 'POST' ||
    method.toUpperCase() === 'PUT')) {
    options = { ...options,
      headers: { ...options.headers },
      body: upload
    }
  }

  if (shouldNeedAuth) {
    const { accessToken } = store.getState().user
    if (accessToken) {
      options = { ...options,
        headers: { ...options.headers,
          Authorization: `Bearer ${accessToken}`
        }
      }
    }
  }

  return fetch(fullUrl, options)
    .then(checkStatus)
    .then(response => response.json())
    .then(data =>
      dispatch({
        type: receiveType,
        payload: data,
        _pointer
      })
    )
    .catch(err => {
      if (err.response && err.response.status === 401) {
        dispatch({ type: failureType, payload: { errors: [] } })
      } else {
        dispatch({
          type: failureType,
          payload: { errors: [{ message: 'Something Error!' }] },
          _pointer
        })
      }
    })
}
